(function($){
  'use strict';

  // Focus no input text
  var focusInput = $('.form .form-text');
  focusInput.focus(function() {
    $(this).parent().addClass('form-focus');
  });
  focusInput.blur(function() {
    if($(this).val() == ""  ){
      $(this).parent().removeClass('form-focus');
    }
  });
  focusInput.each(function() {
    if($(this).val() == ""  ){
      $(this).parent().removeClass('form-focus');
    }else{
      $(this).parent().addClass('form-focus');
    }
  });

  // Focus no select
  var focusSelect = $('.form .form-field-select select');

  focusSelect.change(function() {
    var selectText = $(this).find('option:selected').text();
    var selectLabel = $(this).parent().find('.form-text');
    selectLabel.text("");

    if(selectText == ""  ){
      $(this).parent().removeClass('form-focus');
    }else{
      $(this).parent().addClass('form-focus');
      selectLabel.text(selectText);
    }

  });

  // Focus no file
  var focusFile = $('.form .form-field-file input[type=file]');
  focusFile.change(function() {
    var selectText = $(this).val();
    var selectLabel = $(this).parent().find('.form-text');
    selectLabel.text("");
    if(selectText == ""  ){
      $(this).parent().removeClass('form-focus');
    }else{
      $(this).parent().addClass('form-focus');
      selectLabel.text(selectText);
    }
  });

  $('.mask-phone').focusout(function(){
    var phone, element; element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
      element.mask("(99) 99999.999?9", {autoclear: false});
    } else {
      element.mask("(99) 9999.9999?9", {autoclear: false});
    }
  }).trigger('focusout');

  $(".mask-cep").mask("99999-999");
  $(".mask-date").mask("99/99/9999");

  function checkEmail(email) {
    var exclude=/[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
    var check=/@[\w\-]+\./;
    var checkend=/\.[a-zA-Z]{2,3}$/;
    if(((email.search(exclude) != -1)||(email.search(check)) == -1)||(email.search(checkend) == -1)){return false;}
    else {return true;}
  }



  $('.form').submit(function(){
    var form = $(this);
    var error = false;
    var submitText = form.find('.form-submit').data('text');

    form.find('.form-field').removeClass('error');
    form.find('.msg-success').slideUp(300);

    form.find('.required').each(function(i, e){
      var e = $(e);
      if (!e.val()) {
        e.parent().addClass('error');
        error = true;
      } else if (e.attr('id') == 'email' && !checkEmail(e.val())) {
        e.parent().addClass('error');
        error = true;
      }
    });
    if (error) {
      form.find('.msg-error').slideDown(600);
    } else {
      var fileData = $('#file').get(0);
      var formData = new FormData(this);

      if ($('#file').length > 0) {
        formData.append('file', fileData.files[0]);
      }

      formData.append('json', true);

      $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: formData,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
          form.find('.form-submit').val('Enviando...');
          form.find('.form-submit').prop('disabled', true);
          form.find('.msg-error').slideUp(800);
        },
        complete : function(){
          form.get(0).reset();
          form.find('.form-text').text("");
          form.find('.form-field').removeClass('form-focus');
          form.find('.form-field').removeClass('error');
          form.find('.form-submit').val(submitText);
          form.find('.form-submit').prop('disabled', false);
          form.find('.msg-success').slideDown(800);
        },
        success: function(resp){
          if (resp.success) {
            form.find('.msg-success').slideDown(800);
            setTimeout(function(){
              form.find('.msg-success').slideUp(800);
            },5000);
          } else {
            form.find('.msg-error').slideDown(600);
          }
        },
        xhr: function() {
          var myXhr = $.ajaxSettings.xhr();
          return myXhr;
        }
      });
    }
    return false
  });

})(jQuery);
