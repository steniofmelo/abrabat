<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie8 oldie" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="pt-br"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Abrabat</title>
  <meta name="author"      content="Abrabat">
  <meta name="description" content="">
  <meta name="keywords"    content="">
  <meta name="viewport"    content="width=device-width, initial-scale=1">
  <!-- twitter card -->
  <meta name="twitter:card"        content="summary">
  <meta name="twitter:image"       content="">
  <meta name="twitter:title"       content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:creator"     content="">
  <!-- open graph -->
  <meta property="og:locale"      content="pt_BR">
  <meta property="og:type"        content="website">
  <meta property="og:title"       content="">
  <meta property="og:description" content="">
  <meta property="og:url"         content="">
  <meta property="og:image"       content="">
  <meta property="og:site_name"   content="">
  <!-- add to homescreen for chrome on android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="img/content/chrome-touch-icon-192x192.png">
  <!-- add to homescreen for safari on ios -->
  <meta name="apple-mobile-web-app-capable"          content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title"            content=" ">
  <link rel="apple-touch-icon-precomposed" href="img/content/apple-touch-icon-precomposed.png">
  <!-- tile icon for win8 -->
  <meta name="msapplication-TileImage" content="img/content/ms-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#81cfff">
  <!-- favicon -->
  <link rel="shortcut icon" href="img/content/favicon.ico">
  <link rel="icon"          href="img/content/favicon.ico">
  <!-- styles -->
  <!-- SEO tag href="url atual" / hreflang="" -->
  <link rel="alternate"  href="" hreflang="pt">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <style type="text/css"><?php echo file_get_contents('css/style.css') ?></style>
</head>
<body>
  <nav class="screen-reader">
    <a href="#content" accesskey="c">Alt + Shift + C ir para o conteúdo</a>
    <a href="#nav" accesskey="m">Alt + Shift + M ir para o menu</a>
    <a href="#search" accesskey="b">Alt + Shift + B ir para a busca</a>
    <a href="#footer" accesskey="f">Alt + Shift + F ir para o rodapé</a>
  </nav><!-- .screen-reader -->
  
  <div class="wrap">
    <header class="header internal">
      <div class="border"><div class="inner"></div></div>

      <a href="#" class="brand"><img src="img/layout/logo_abrabat.png" alt="Abrabat" /></a>
      
      <nav class="menu">
      <a href="" class="button-mobile">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </a><!-- btn mobile -->
        
        <ul>
          <li><a href="">Página Inicial</a></li>
          <li><a href="">Notícias</a></li>
          <li><a href="">Perguntas Frequentes</a></li>
          <li><a href="">Contato</a></li>
        </ul>
      </nav>

      <div class="title-page">
        <h1>Notícias</h1>
      </div>
    </header><!-- .header -->
    
    <section class="content">

      <div class="container">
        
        <div class="page-news">
          <div class="posts">
            <div class="post">
              <a href="#">
                <div class="header-post">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                  
                </div>
                <div class="content-post">
                  <img src="https://via.placeholder.com/370x230" alt="">
                  <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  <span class="more-news">Leia mais</span>
                </div>
              </a>
            </div><!-- post -->

            <div class="post">
              <a href="#">
                <div class="header-post">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                  
                </div>
                <div class="content-post">
                  <img src="https://via.placeholder.com/370x230" alt="">
                  <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  <span class="more-news">Leia mais</span>
                </div>
              </a>
            </div><!-- post -->

            <hr />

            <div class="post">
              <a href="#">
                <div class="header-post">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                  
                </div>
                <div class="content-post">
                  <img src="https://via.placeholder.com/370x230" alt="">
                  <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  <span class="more-news">Leia mais</span>
                </div>
              </a>
            </div><!-- post -->

            <div class="post">
              <a href="#">
                <div class="header-post">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                  
                </div>
                <div class="content-post">
                  <img src="https://via.placeholder.com/370x230" alt="">
                  <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  <span class="more-news">Leia mais</span>
                </div>
              </a>
            </div><!-- post -->

            <hr />

            <div class="post">
              <a href="#">
                <div class="header-post">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                  
                </div>
                <div class="content-post">
                  <img src="https://via.placeholder.com/370x230" alt="">
                  <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  <span class="more-news">Leia mais</span>
                </div>
              </a>
            </div><!-- post -->

            <div class="post">
              <a href="#">
                <div class="header-post">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                  
                </div>
                <div class="content-post">
                  <img src="https://via.placeholder.com/370x230" alt="">
                  <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  <span class="more-news">Leia mais</span>
                </div>
              </a>
            </div><!-- post -->

            <a href="" class="btn btn-green more">Carregar mais <i class="icon icon-loader"></i></a>
          </div><!-- posts -->

          <div class="sidebar">
            <section>
              <div class="widget">
                <div class="content-widget">
                  <h2 class="widget-title">Notícias em destaque</h2>
                  <div class="posts">
                    <div class="post">
                      <a href="#">
                        <div class="header-post">
                          <span class="date"><b>24</b>Junho, 2019</span>
                          <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                          
                        </div>
                        <div class="content-post">
                          <img src="https://via.placeholder.com/370x230" alt="">
                        </div>
                      </a>
                    </div><!-- posts -->

                    <div class="post">
                      <a href="#">
                        <div class="header-post">
                          <span class="date"><b>24</b>Junho, 2019</span>
                          <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                          
                        </div>
                        <div class="content-post">
                          <img src="https://via.placeholder.com/370x230" alt="">
                        </div>
                      </a>
                    </div><!-- posts -->
                  </div>
                </div>

              </div>
            </section>

            <div class="widget categories">
              <div class="content-widget">
                <h2 class="widget-title">Categorias</h2>

                <ul>
                  <li><a href="#">Assossiação </a></li>
                  <li><a href="#">Notícias de Negócios</a></li>
                  <li><a href="#">Tecnologia Mundial</a></li>
                  <li><a href="#">Mundo da Bateria</a></li>
                  <li><a href="#">Brasil</a></li>
                  <li><a href="#">Argentina</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div><!-- news -->
      </div>
      
    </section><!-- .content -->

    <footer class="footer">
      <div class="border"><div class="inner"></div></div>

      <img src="img/layout/logo_abrabat_oval.png" alt="" class="logo-footer" />

      <ul class="menu-footer">
        <li><a href="">Produtos Certificados</a></li>
        <li><a href="">Obrigações do Comerciante</a></li>
        <li><a href="">Obrigações do Fabricante</a></li>
        <li><a href="">Direitos do Consumidor</a></li>
      </ul>
    </footer><!-- .footer -->
  </div><!-- .wrap -->

  <!-- scripts -->
  <script src="js/lib/modernizr.js"></script>
  <script src="js/lib/jquery-3.2.1.min.js"></script>
  <script src="js/app/main.js"></script>
</body>
</html>
